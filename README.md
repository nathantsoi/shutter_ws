# Shutter Tarzan Workspace

ROS provides message passing and robot control. Docker provides an isolated way to run ROS regardless of your linux machine type. This is only tested on Ubuntu 18.04.

## Quick Start

Fork this repository and clone to your user's home directory, into the `tarzan_ws` folder:

```
cp
git clone git@gitlab.com:[yourusername]/tarzan_ws.git
cd tarzan_ws
./scripts/checkout.sh
```

### To install all dependencies without Docker:

./scripts/install.sh

Ubuntu 18.04 is supported (arm64 or intel)

### Or continue to the docker configuration

- Docker config

  - Note that audio inputs and output does not work in docker

  - Note that nvidia-docker2 requires a compatible GPU (graphics card). If you are not using container images that require nvidia-docker2, you do not need these hardware and software dependencies.

  - Built on ROS Melodic for Ubuntu 18.04

  - Wrapped in `docker-compose` to make portable compiling and running a bit easier

## Docker Dependencies

See the [Detailed Setup Instructions](#detailed-setup-instructions) for step-by-step setup instructions

  - [nvidia-docker2](https://github.com/nvidia/nvidia-docker/wiki/Installation-(version-2.0)) - [docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce)
  - [docker-compose](https://docs.docker.com/compose/install/#install-compose)

  - [yarn](https://yarnpkg.com/en/docs/install): for managing environmental vars and scripts


## Development

Use either tmuxinator or start manually.

### [Tmuxinator](https://github.com/tmuxinator/tmuxinator)

  - [install tmuxinator](https://github.com/tmuxinator/tmuxinator#installation) and run `tmuxinator start tarzan_ws`

  - start tarzan workspace with `tmuxinator start tarzan -p tmuxinator/tarzan.yaml`

### Manual

  - run `yarn build`

  - wait a while for everything to build the first time (~10 min)

  - run `yarn shell`

    - this will start a ROS master node and open a shell

    - `yarn shell` can be run multiple times, once per shell/binary

    - the shell will attempt to `source devel/setup.bash` but in the case that it does not yet exist, e.g. before running `catkin_make` you may have to source this yourself

  - to kill all nodes run `yarn kill`

## Detailed Setup Instructions

Install [docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce)

```
sudo apt install -y curl
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common \
    docker-ce
```

Install [nvidia-docker2](https://github.com/NVIDIA/nvidia-docker#ubuntu-140416041804-debian-jessiestretch):

```
# Add the package repositories
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | \
  sudo apt-key add -
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | \
  sudo tee /etc/apt/sources.list.d/nvidia-docker.list
sudo apt-get update

# Install nvidia-docker2 and reload the Docker daemon configuration
sudo apt-get install -y nvidia-docker2
sudo pkill -SIGHUP dockerd

# Test nvidia-smi with the latest official CUDA image
docker run --runtime=nvidia --rm nvidia/cuda:10.0-base nvidia-smi
```

Install [Docker Compose](https://docs.docker.com/compose/install/#install-compose)

```
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

If you get an error than nvidia is not a known runtime, make sure `/etc/docker/daemon.json` contains:

```
{
...
  "runtimes": {
    "nvidia": {
      "path": "/usr/bin/nvidia-container-runtime",
      "runtimeArgs": []
    }
  }
...
}
```
