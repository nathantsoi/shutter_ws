#!/bin/bash

###
# Check out required code
###

#set -x
set -e

SRC=$HOME/$WORKSPACENAME/src
mkdir -p $SRC
pushd $SRC

# clone a target repo if it doesn't exist, other wise prints "Skipping" and continues
clone_or_warn () {
  l=$#
  to_clone="${!l}"
  echo $to_clone
  bn=$(basename "$to_clone" .git)
  if [ -d "$bn" ]; then
    echo "Skipping $to_clone, $bn already exists"
  else
    git clone $@
  fi
}

# Dependencies
clone_or_warn --recursive git@gitlab.com:interactive-machines/shutter/shutter-ros.git
clone_or_warn --recursive git@gitlab.com:interactive-machines/shutter/shutter-tarzan.git
clone_or_warn --recursive https://github.com/Slamtec/rplidar_ros.git

popd
