#!/bin/bash

set -x
set -e

sudo apt-get install --no-install-recommends -y \
  curl
  python-catkin-tools \
  python-rosinstall-generator \
  python-rosdep \
  python-rosinstall \
  python-vcstools \
  apt-utils \
  wget \
  sudo \
  vim \
  xauth \
  iproute2 \
  net-tools \
  alsa-utils

# ROS
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
sudo apt update
sudo rosdep init && rosdep update

ROS_USER=shutter
ROS_WORKSPACE=tarzan
ROS_DISTRO=melodic

# install ros packages
sudo apt-get install -y \
    ros-${ROS_DISTRO}-ros-core \
    ros-${ROS_DISTRO}-ros-base \
    ros-${ROS_DISTRO}-robot \
    ros-${ROS_DISTRO}-desktop \
    ros-${ROS_DISTRO}-desktop-full

# workspace specific dependencies
sudo apt-get install -y \
    libatlas-base-dev \
    libjpeg-dev \
    libpng-dev \
    libblas-dev \
    libgfortran-4.8-dev \
    libz-dev \
    libpcap-dev \
    libsvm-dev \
    libcanberra-gtk-module \
    libasound-dev \
    pcl-tools \
    python-webcolors \
    python-pygame \
    python-pip \
    python-pyaudio \
    python3-pip \
    python3-setuptools \
    python3.7-venv \
    xsltproc \
    "ros-${ROS_DISTRO}-openni2-*" \
    ros-${ROS_DISTRO}-usb-cam \
    ros-${ROS_DISTRO}-rosbash \
    ros-${ROS_DISTRO}-tf2-sensor-msgs \
    ros-${ROS_DISTRO}-tf2-tools \
    ros-${ROS_DISTRO}-rosbridge-suite \
    ros-${ROS_DISTRO}-web-video-server \
    ros-${ROS_DISTRO}-vision-opencv \
    ros-${ROS_DISTRO}-cv-bridge \
    ros-${ROS_DISTRO}-joint-state-controller \
    "ros-${ROS_DISTRO}-pcl-*" \
    ros-${ROS_DISTRO}-video-stream-opencv \
    ros-${ROS_DISTRO}-rgbd-launch \
    ros-${ROS_DISTRO}-openni2-camera \
    ros-${ROS_DISTRO}-ddynamic-reconfigure \
    ros-${ROS_DISTRO}-rqt \
    ros-${ROS_DISTRO}-rqt-common-plugins \
    ros-${ROS_DISTRO}-rqt-robot-plugins \
    ros-${ROS_DISTRO}-opencv-apps \
    ros-${ROS_DISTRO}-joy

# librealsense
AARCH=$(uname -m)
if ["$AARCH" =~ "aarch64"]; then
  # https://github.com/IntelRealSense/librealsense/blob/master/doc/installation.md
  pushd /usr/src
  sudo git clone https://github.com/IntelRealSense/librealsense.git
  sudo chmod 777 librealsense
  pushd librealsense
  git checkout -b origin/v2.9.1
  sudo apt update && sudo apt install -y git cmake libssl-dev \
    libusb-1.0-0-dev pkg-config libgtk-3-dev \
    libglfw3-dev \
    libudev-dev \
    cmake-curses-gui
  sudo cp config/99-realsense-libusb.rules /etc/udev/rules.d/
  sudo udevadm control --reload-rules && udevadm trigger
  mkdir -p build && mkdir -p install
  pushd build
  cmake ../ -DBUILD_EXAMPLES=true -DCMAKE_BUILD_TYPE=release -DBUILD_UNIT_TESTS=false
  make -j6
  sudo make install
  popd
  popd
  popd
else
  sudo apt-key adv --keyserver keys.gnupg.net --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE || sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE
  sudo add-apt-repository "deb http://realsense-hw-public.s3.amazonaws.com/Debian/apt-repo bionic main" -u
  sudo apt update
  sudo apt-get install -y librealsense2-dkms librealsense2-utils librealsense2-dev librealsense2-dbg
fi

# groups
sudo adduser ${ROS_USER} dialout

printf "source /opt/ros/${ROS_DISTRO}/setup.zsh\n\
FILE=/home/${ROS_USER}/${ROS_WORKSPACE}/devel/setup.zsh && test -f \$FILE && source \$FILE || true\n\
export PATH=/sbin/:\$PATH" >> /home/${ROS_USER}/.zshrc

# tx2 clocks
if ["$AARCH" =~ "aarch64"]; then
printf '#!/bin/bash
echo 1 > /sys/devices/system/cpu/cpu1/online
echo 1 > /sys/devices/system/cpu/cpu2/online
jetson_clocks' | sudo tee /usr/bin/powerup
sudo chmod +x /usr/bin/powerup
echo "@reboot /usr/bin/powerup" | sudo crontab -
fi
